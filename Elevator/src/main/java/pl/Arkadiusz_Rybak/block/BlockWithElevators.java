package pl.Arkadiusz_Rybak.block;

import pl.Arkadiusz_Rybak.Addres.Addres;
import pl.Arkadiusz_Rybak.Elevator.Elevator;
import pl.Arkadiusz_Rybak.Floor.Floor;
import pl.Arkadiusz_Rybak.Person.ElevatorUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BlockWithElevators extends Bloock {

   private List<Elevator> mElevetors;

   private HashMap<Floor, List<ElevatorUser>> mElevatorsQueue;  //kluczem jest floor a wartoscia jest lista z urzytkownikami windy.Reprezentuje liste osob ktorzy chca sie przejechac winda



    public BlockWithElevators(List<Floor> mFloor) {
        super(mFloor);
        mElevatorsQueue = new HashMap<>();
    }


    public List<Elevator> getmElevetors() {
        return mElevetors;
    }

    public void setmElevetors(List<Elevator> mElevetors) {
        this.mElevetors = mElevetors;
    }

    public HashMap<Floor, List<ElevatorUser>> getmElevatorsQueue() {
        return mElevatorsQueue;
    }

    public void setmElevatorsQueue(HashMap<Floor, List<ElevatorUser>> mElevatorsQueue) {
        this.mElevatorsQueue = mElevatorsQueue;
    }

    //dodawanie klienta windy
    public void addElevatorUserToQueue(Floor aFloor, ElevatorUser aElevatorUser) {


        List<ElevatorUser> listOfQueueOnFloor = mElevatorsQueue.get(aFloor);

        if (listOfQueueOnFloor != null) { //sprawdza czy lista jest pusta
            System.out.println("Dodajemy urzytkownika do istniejącego piętra");
            listOfQueueOnFloor.add(aElevatorUser); //jezeli jest to dodaje urzytkownika windy
        } else {
            System.out.println("nie mamy piętra więc je tworzymy i dodajemy urzytkownika ");
            ArrayList<ElevatorUser> elevatorUsers = new ArrayList<>();
            elevatorUsers.add(aElevatorUser);
            mElevatorsQueue.put(aFloor, elevatorUsers); //dodajemy do hasz mapy klucz
        }
             }

//        for (int i=0; i<mElevatorsQueue.size(); i++){
//            mElevatorsQueue.keySet().add(setmFloor());
//        }




    @Override
    public boolean hasElevator() {
        return true;
    }
}
