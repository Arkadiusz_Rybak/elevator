package pl.Arkadiusz_Rybak.block;

import pl.Arkadiusz_Rybak.Addres.Addres;
import pl.Arkadiusz_Rybak.Floor.Floor;

import java.util.List;

public abstract class Bloock {

    private List<Floor>mFloor;
    private Addres addres;

    public Bloock(List<Floor> mFloor) {
        this.mFloor = mFloor;

    }

    public abstract boolean hasElevator();

    public List<Floor> getmFloor() {
        return mFloor;
    }

    public void setmFloor(List<Floor> mFloor) {
        this.mFloor = mFloor;
    }

    public Addres getAddres() {
        return addres;
    }

    public void setAddres(Addres addres) {
        this.addres = addres;
    }

}
