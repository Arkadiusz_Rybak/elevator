package pl.Arkadiusz_Rybak.Addres;

public class Addres {

    private City mCity;
    private String mStreetName;
    private int mBlockNo;
    private int mApartamentNo;
    private String mPostCode;

    public Addres(City mCity, String mStreetName, int mBlockNo, int mApartamentNo, String mPostCode) {
        this.mCity = mCity;
        this.mStreetName = mStreetName;
        this.mBlockNo = mBlockNo;
        this.mApartamentNo = mApartamentNo;
        this.mPostCode = mPostCode;
    }

    public City getmCity() {
        return mCity;
    }

    public void setmCity(City mCity) {
        this.mCity = mCity;
    }

    public String getmStreetName() {
        return mStreetName;
    }

    public void setmStreetName(String mStreetName) {
        this.mStreetName = mStreetName;
    }

    public int getmBlockNo() {
        return mBlockNo;
    }

    public void setmBlockNo(int mBlockNo) {
        this.mBlockNo = mBlockNo;
    }

    public int getmApartamentNo() {
        return mApartamentNo;
    }

    public void setmApartamentNo(int mApartamentNo) {
        this.mApartamentNo = mApartamentNo;
    }

    public String getmPostCode() {
        return mPostCode;
    }

    public void setmPostCode(String mPostCode) {
        this.mPostCode = mPostCode;
    }
    @Override
    public String toString(){
        return mCity.getName()+ " " + mStreetName + " " + mBlockNo+ " "+ mApartamentNo + " "+ mPostCode;
    }
}
