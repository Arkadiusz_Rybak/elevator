package pl.Arkadiusz_Rybak.Addres.utils;

import com.sun.istack.internal.Nullable;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final public class PostalCode { //finalna klasa - nic nie moze po niej dziedziczyc

    private PostalCode(){
        //do nothing
    }

    private String aCode;
    private String aRegion;

    public static boolean isCorrect (String aCode, @Nullable String aRegion){

//        Pattern p = Pattern.compile("\\d\\d-\\d\\d\\d");
//        Matcher m = p.matcher(aCode);
//        boolean b = m.matches();
        Locale locale = Locale.getDefault(); //wywolanie na klasie metody getdif wywoluje dana lokalizacje np jestes w Polsce wywoluje wlasciwosci dla Polski

        String iso3Country = locale.getISO3Country(); //3 literowe ISO
        Pattern pattern = null;
        switch (aRegion){
            case"POL":
                pattern = Pattern.compile("\\d\\d-\\d\\d\\d");
                break;
            case"USA":
                pattern = Pattern.compile("\\d\\d\\d\\d\\d");
                break;
            }
            Matcher matcher = pattern.matcher(aCode); //porownujemy patern wzgledem stringa ktory chcemy porownac
        return matcher.matches();
        }
    }


