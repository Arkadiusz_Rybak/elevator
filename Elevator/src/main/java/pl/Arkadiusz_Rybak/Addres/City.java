package pl.Arkadiusz_Rybak.Addres;

public enum City {

    LUBLN ("Lublin"),
    WARSZAWA("Warszawa"),
    GDAŃSK("Gdańsk"),
    ŁÓDŹ("Łódź"),
    STALOWAWOLA("Stalowa Wola"),
    KATOWICE("Katowice");



    private String cityName;

    City (String name){
    this.cityName = name;
    }

    public String getName() {
        return cityName;
    }
    @Override
    public String toString(){
        return cityName;
    }

}
