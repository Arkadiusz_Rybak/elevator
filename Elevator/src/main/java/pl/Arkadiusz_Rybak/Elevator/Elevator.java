package pl.Arkadiusz_Rybak.Elevator;

import com.sun.javafx.scene.traversal.Direction;
import pl.Arkadiusz_Rybak.Floor.Floor;
import pl.Arkadiusz_Rybak.weight.WeighMetrics;
import pl.Arkadiusz_Rybak.weight.Weight;

import java.util.ArrayList;
import java.util.List;

public class Elevator {

    private List<Floor> mAvailableFloors;

    private List<Floor> mRequestFloors = new ArrayList<>();

    private Floor mCurrentFloor;

    private boolean mServiceBreak = false;

    private Weight mMaxLoadKG = new Weight(600, WeighMetrics.KG);

    Direction mDirection = Direction.DOWN ;

    private Weight mMaxLoad;

    public Weight getmMaxLoad() {
        return mMaxLoad;
    }

    public List<Floor> getmAvailableFloors() {
        return mAvailableFloors;
    }

    public void setmAvailableFloors(List<Floor> mAvailableFloors) {
        this.mAvailableFloors = mAvailableFloors;
    }

    public List<Floor> getmRequestFloors() {
        return mRequestFloors;
    }

    public void setmRequestFloors(List<Floor> mRequestFloors) {
        this.mRequestFloors = mRequestFloors;
    }

    public Floor getmCurrentFloor() {
        return mCurrentFloor;
    }

    public void setmCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }

    public boolean ismServiceBreak() {
        return mServiceBreak;
    }

    public void setmServiceBreak(boolean mServiceBreak) {
        this.mServiceBreak = mServiceBreak;
    }

    public Weight getmMaxLoadKG() {
        return mMaxLoadKG;
    }

    public void setmMaxLoadKG(Weight mMaxLoadKG) {
        this.mMaxLoadKG = mMaxLoadKG;
    }

    public Direction getmDirection() {
        return mDirection;
    }

    public void setmDirection(Direction mDirection) {
        this.mDirection = mDirection;
    }

    public void getmMaxLoadKG(Weight weight, WeighMetrics kg) {

    }

    public void getmAvailableFloors(List<Floor> floors) {

    }

    public float setmMaxLoad(Weight mMaxLoad) {
        this.mMaxLoad = mMaxLoad;
        return 0;
    }
}
