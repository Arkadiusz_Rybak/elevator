package pl.Arkadiusz_Rybak.Elevator;

import pl.Arkadiusz_Rybak.Person.ElevatorUser;
import pl.Arkadiusz_Rybak.weight.WeighMetrics;
import pl.Arkadiusz_Rybak.weight.Weight;

import java.util.ArrayList;
import java.util.List;


public class PassengerElevator extends Elevator {

    List<ElevatorUser>mPersonOnBoard = new ArrayList<>();
    short mMaxLoadPerson = 13;


    @Override
    public float setmMaxLoad(Weight mMaxLoadKg){
        super.setmMaxLoad(mMaxLoadKg);

        int maxWeight = mMaxLoadKg.getmAmount();
        float safeWeigh = (float) (getmMaxLoad().getmAmount() * 0.9);

        mMaxLoadPerson = (short) (maxWeight/90);
        if(mMaxLoadKg.getmWeightMetrics() == WeighMetrics.KG){
            mMaxLoadPerson =(short) (maxWeight * 0.45);
        }else{
            mMaxLoadPerson = (short) (maxWeight * 2.2);
        }
        return maxWeight;
    }
}
