package pl.Arkadiusz_Rybak.weight;

public class Weight {


    private int mAmount;
    private WeighMetrics mWeightMetrics;

    public Weight(int aAmount, WeighMetrics aWeighMetrics) {
        this.mAmount = aAmount; //ilosc
        this.mWeightMetrics = aWeighMetrics;
    }



    public int getmAmount() {
        return mAmount;
    }

    public void setmAmount(int mAmount) {
        this.mAmount = mAmount;
    }

    public WeighMetrics getmWeightMetrics() {
        return mWeightMetrics;
    }

    public void setmWeightMetrics(WeighMetrics mWeightMetrics) {
        this.mWeightMetrics = mWeightMetrics;
    }

    public Weight(int aAmount, String kg) {

    }

    //ma zwracac aktualna wage w podanej metryce

    public float getWeight(WeighMetrics aWeightMetrics) {

        if (aWeightMetrics == WeighMetrics.KG) {
            return (float) (mAmount * 0.45f);
        }else if (aWeightMetrics == WeighMetrics.LB){
            return (float) (mAmount * 2.2);
        }
        return mAmount;
    }
    @Override
    public String toString(){
        return mAmount+ " "+mWeightMetrics; //aby wyswietlic LB albo KG
    }
}



