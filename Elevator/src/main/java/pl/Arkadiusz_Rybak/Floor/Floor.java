package pl.Arkadiusz_Rybak.Floor;

import pl.Arkadiusz_Rybak.Person.ElevatorUser;

public class Floor {

    int mFloorNumber;

    public Floor(int mFloorNumber) {
        this.mFloorNumber = mFloorNumber;
    }

    public int getmFloorNumber() {
        return mFloorNumber;
    }

    public void setmFloorNumber(int mFloorNumber) {
        this.mFloorNumber = mFloorNumber;
    }


    @Override
    public boolean equals (Object obj){
        boolean result = false;

        if ( obj instanceof Floor){
            Floor temp = (Floor) obj;
            if(mFloorNumber == temp.mFloorNumber)
                result = true;
        }
        return  result;
    }

    @Override
    public String toString(){
        return mFloorNumber+" ";
    }
}
