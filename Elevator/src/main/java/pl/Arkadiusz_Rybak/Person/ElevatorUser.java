package pl.Arkadiusz_Rybak.Person;

import pl.Arkadiusz_Rybak.Addres.Addres;
import pl.Arkadiusz_Rybak.Floor.Floor;
import pl.Arkadiusz_Rybak.Person.Person;
import pl.Arkadiusz_Rybak.weight.Weight;

public class ElevatorUser extends Person{

    private boolean isInElevator;
    private Floor mWantToGoFloor;
    private Floor mCurrentFloor;

    public ElevatorUser(String firstName, String name, Addres mAdress, Weight mWeight, boolean isInElevator, Floor mWantToGoFloor, Floor mCurrentFloor) {
        super(firstName, name, mAdress, mWeight);
        this.isInElevator = isInElevator;
        this.mWantToGoFloor = mWantToGoFloor;
        this.mCurrentFloor = mCurrentFloor;
    }

    public ElevatorUser() {

    }

    public boolean isInElevator() {
        return isInElevator;
    }

    public void setInElevator(boolean inElevator) {
        isInElevator = inElevator;
    }

    public Floor getmWantToGoFloor() {
        return mWantToGoFloor;
    }

    public void setmWantToGoFloor(Floor mWantToGoFloor) {
        this.mWantToGoFloor = mWantToGoFloor;
    }

    public Floor getmCurrentFloor() {
        return mCurrentFloor;
    }

    public void setmCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }
//        @Override
//    public String toString(){
//        return isInElevator + " "+ mWantToGoFloor+ " "+ mCurrentFloor;
//    }


    @Override
    public String toString() {
        return super.toString() +" "+isInElevator+" "+mWantToGoFloor+" "+mCurrentFloor;  //nadpisuje po person (dziedziczy)
    }
}
