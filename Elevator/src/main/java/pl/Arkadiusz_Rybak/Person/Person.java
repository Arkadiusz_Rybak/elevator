package pl.Arkadiusz_Rybak.Person;

import pl.Arkadiusz_Rybak.Addres.Addres;
import pl.Arkadiusz_Rybak.weight.Weight;

public class Person {

    private String FirstName;
    private String Name;
    private Addres mAdress;
    private Weight mWeight;

    public Person(String firstName, String name, Addres mAdress, Weight mWeight) {
        FirstName = firstName;
        Name = name;
        this.mAdress = mAdress;
        this.mWeight = mWeight;
    }

    public Person() {

    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Addres getmAdress() {
        return mAdress;
    }

    public void setmAdress(Addres mAdress) {
        this.mAdress = mAdress;
    }

    public Weight getmWeight() {
        return mWeight;
    }

    public void setmWeight(Weight mWeight) {
        this.mWeight = mWeight;
    }
    @Override
    public String toString(){
        return " DANE PERSONALNE "+FirstName+" "+Name+" ADRES "+mAdress+" Waga "+mWeight;
    }
}
